# -*- coding: utf-8 -*-

import requests
from bs4 import BeautifulSoup as BS

page_response = requests.get("http://www.thedailystar.net/")

page = BS(page_response.text,'lxml')

def get_all_headings():
    all_headings = page.find_all("h1")+page.find_all("h2")+page.find_all("h3")+page.find_all("h4")+page.find_all("h5")+page.find_all("h6")
    ret = []
    for heading in all_headings:
        if bool(heading.text.strip()) == True:    
            #print(len(heading.text.strip()))        
            #print(heading.text.strip())
            ret.append(heading.text.strip())
    return ret   
    
def print_all_categories(ret):
    #ret = get_all_headings()
    for item in ret:
        print(item)
        

#print(*get_all_headings(),sep='\n')

print_all_categories(get_all_headings())
